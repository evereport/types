module.exports = {
	base: require('./lib/classes/modules/base'),
	data: {
		test: require('./lib/classes/modules/Test'),
		suite: require('./lib/classes/modules/Suite'),
		run: require('./lib/classes/modules/Run'),
	},
	states: require('./lib/states'),
	schemas: require('./lib/schemas')
};