const joi = require('@hapi/joi');

/**
 * @description
 * Data class with validation that can be included as data part of other class
 * @class
 */
module.exports = class Module {
	data;

	constructor(data, validate = false) {
		if (validate)
			this.constructor.validate(data);
		this.data = data;
		Object.freeze(this.data);
	}

	/**
	 * @description
	 * Validates given object;
	 * @param {*} data
	 * @returns {*}
	 * @throws {ValidationError}
	 */
	static validate(data) {
		this._schema = this.__schema;
		const { error } = this._schema.validate(data);
		if (error)
			throw error;
		return data;
	}
	/**
	 * @description
	 * Joi schema
	 * @type {joi}
	 * @abstract
	 * @static
	 */
	static schema = joi.object({});
	/**
	 * @description
	 * Extended schema
	 * @type {joi}
	 * @static
	 * @private
	 */
	static _schema;
	static get __schema() {
		let c;
		if (!this._schema) {
			const keys = this.modules.reduce((o, m) => {
				const field = m._as ? m.as : m.field;
				const module = m._as ? m.$ : m;
				o[field] = (module.post) ? module.build() : module.__schema;
				return o;
			}, {});
			c = (Object.keys(keys).length) ? this.schema.keys(keys) : this.schema;
			if (this.id)
				c = c.id(this.id);
		}
		return (this._schema) ? this._schema : c;
	}
	/**
	 * @description
	 * Other modules included in this one
	 * @type {Array<Module>}
	 * @abstract
	 * @static
	 */
	static modules = [];
	/**
	 * @description
	 * Used to including modules to other modules
	 * @type {string}
	 * @abstract
	 * @static
	 */
	static field = 'module';
	/**
	 * @description
	 * Shows if module needs to be built
	 * @type {boolean}
	 * @static
	 */
	static post = false;
	/**
	 * @description
	 * Returns built schema
	 * @abstract
	 * @static
	 */
	static build() {
		return this.__schema;
	}
	/**
	 * @description
	 * Include module as different field
	 * @param {string} field - As
	 * @return {object}
	 */
	static as(field) {
		return { _as: field, $: this };
	}
	/**
	 * @type {string}
	 */
	static id;
};