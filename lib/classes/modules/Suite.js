const joi = require('@hapi/joi');
const Entity = require('./Entity');
const Test = require('./Test');
const { summary } = require('./base');

const suite = module.exports = class Suite extends Entity {
	static modules = Array.of(...super.modules, summary);
	static schema = super.schema.keys({
		tree: joi.object().keys({
			suites: joi.array().items(joi.link('....')),
			tests: joi.array().items(Test.__schema)
		}).required()
	});
	static id = 'suite';
};
