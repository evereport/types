const Entity = require('./Entity');
const { error } = require('./base');
const { uuid } = require('../../schemas');

module.exports = class Test extends Entity {
	static modules = Array.of(...super.modules, error);
	static schema = super.schema.keys({
		parentId: uuid
	})
};
