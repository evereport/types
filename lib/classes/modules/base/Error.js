const joi = require('@hapi/joi');
const Module = require('../../Module');

module.exports = class Error extends Module {
	static schema = joi.object().keys({
		name: joi.string(),
		message: joi.string(),
		stack: joi.string().required(),
	});
	static field = 'error'
};