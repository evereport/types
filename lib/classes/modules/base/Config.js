const joi = require('@hapi/joi');
const Module = require('../../Module');

module.exports = class Config extends Module {
	static field = 'config';
	static schema = joi.object({
		timeout: joi.number().integer(),
		step: joi.boolean(),
		skip: joi.boolean(),
		parallel: joi.boolean(),
	}).required()
};
