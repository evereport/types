const joi = require('@hapi/joi');
const Module = require('../../Module');
const { stringArr } = require('../../../schemas');

module.exports = class Filter extends Module {
	static field = 'filter';
	static schema = joi.object().keys({
		project_id: joi.number().integer().min(0),
		scope: stringArr,
		epic: stringArr,
		feature: stringArr,
		story: stringArr
	}).required();
};
