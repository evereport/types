const joi = require('@hapi/joi');
const Module = require('../../Module');
const states = require('../../../states');

const stat = joi.object().keys(
	Object.values(states).reduce((o, s) => {
		o[s] = joi.number().integer();
		return o;
	}, {})
);

module.exports = class Summary extends Module {
	static schema = joi.object().keys({
		own: joi.object().keys({
			tests: stat.required(),
			suites: stat.required(),
		}),
		summary: joi.object().keys({
			tests: stat.required(),
			suites: stat.required(),
		}),
	}).required();
	static field = 'stats';
};