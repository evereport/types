const joi = require('@hapi/joi');
const Module = require('../Module');
const { uuid, state } = require('../../schemas');
const { config, filter } = require('./base');

/**
 * @description
 * Base class for other entities
 * @class
 * @augments Module
 */
module.exports = class Entity extends Module {
	static schema = joi.object().keys({
		id: uuid,
		title: joi.string().required(),
		state,
		parentId: uuid.optional(),
		runId: uuid,
		timing: joi.number(),
	});

	static modules = [config, filter];
};