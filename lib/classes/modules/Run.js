const joi = require('@hapi/joi');
const Entity = require('./Entity');
const Suite = require('./Suite');
const Test = require('./Test');
const { error, summary } = require('./base');

module.exports = class Run extends Suite {
	static modules = [summary, error];
	static schema = super.schema.keys({
		tree: joi.object().keys({
			tests: joi.array().items(Test.__schema),
			suites: joi.array().items(Suite.__schema),
		}),
		parentId: joi.any().allow(null),
	});
	static id = 'run';
};