const joi = require("@hapi/joi");
const states  = require('./states');

module.exports.uuid = joi.string().guid({ version: "uuidv4" }).required();
module.exports.stringArr = joi.array().items(joi.string()).min(0).allow(null);
module.exports.state = joi.string().valid(...Object.values(states)).required();
